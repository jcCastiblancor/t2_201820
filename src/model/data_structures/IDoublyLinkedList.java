package model.data_structures;

/**
 * Abstract Data Type for a doubly-linked list of generic objects
 * This ADT should contain the basic operations to manage a list
 * add, addAtEnd, AddAtK, getElement, getCurrentElement, getSize, delete, deleteAtK
 * next, previous
 * @param <T>
 */
public interface IDoublyLinkedList<T> extends Iterable<T> {

	Integer getSize();

	public void add(Nodo<T> pNodo);

	public void addAtEnd (Nodo<T> pNodo);

	public void addAtK(Nodo<T> pNodo, int k);

	public Nodo<T> getElement(T obj);

	public Nodo<T> getCurrentElement(T obj);

	public void delete(T obj);

	public void deleteAtK(int k);

	public Nodo<T> next();

	public Nodo<T> previous();
}
