package model.data_structures;

import java.util.Iterator;

@SuppressWarnings({"unchecked", "rawtypes" })
public class DoublyLinkedList <T extends Comparable<T>> implements IDoublyLinkedList<T>{

	//----------------------------------------------------------------------
	// Atributos.
	//----------------------------------------------------------------------

	/**
	 * Primer Nodo.
	 */
	private Nodo<T> primerNodo;

	private Nodo<T> current;

	//----------------------------------------------------------------------
	// Constructor.
	//----------------------------------------------------------------------

	public DoublyLinkedList() {
		primerNodo = null;
		current = null;
	}

	//----------------------------------------------------------------------
	// Metodos.
	//----------------------------------------------------------------------

	public Nodo<T> darPrimerNodo() {
		return primerNodo;
	}

	public Iterator<T> iterator() {	
		return new ListIterator();
	}

	private class ListIterator implements Iterator <T>{

		private Nodo actual = primerNodo;
		public boolean hasNext() {
			return actual !=null;
		}

		public T next() {
			T item =  (T) actual.darObj();
			actual = actual.darSiguiente();
			return item;
		}
		public void remove() {		}

	}

	public Integer getSize() {
		Integer i = 0;
		Nodo<T> actual = primerNodo;
		while (actual!=null) {
			i++;
			actual = actual.darSiguiente();
		}
		return i;
	}

	public void add(Nodo<T> pNodo) {
		Nodo<T> actual = primerNodo;
		primerNodo = pNodo;
		primerNodo.cambiarSiguiente(actual);
		if (actual != null) {
			actual.cambiarAnterior(primerNodo);
		}
	}

	public void addAtEnd(Nodo<T> pNodo) {
		Nodo<T> actual = primerNodo;
		boolean agregado = false;
		if (actual == null) {
			primerNodo = pNodo;
		}else {
			while (actual!=null && !agregado) {
				if (actual.darSiguiente()==null) {
					actual.cambiarSiguiente(pNodo);
					pNodo.cambiarAnterior(actual);
					agregado = true;
				}
				actual = actual.darSiguiente();
			}
		}
	}

	public void addAtK(Nodo<T> pNodo, int k) {
		Nodo<T> actual = primerNodo;
		boolean agregado = false;
		int i = 0;

		while (actual!=null && !agregado) {
			if (i==k) {
				Nodo<T> siguiente = actual.darSiguiente();
				actual.cambiarSiguiente(pNodo);
				pNodo.cambiarAnterior(actual);
				pNodo.cambiarSiguiente(siguiente);
				siguiente.cambiarAnterior(pNodo);
				agregado = true;
			}
			i++;
			actual = actual.darSiguiente();
		}
	}

	public Nodo<T> getElement(T obj) {
		Nodo<T> actual =null;
		if (obj!=null) {
			actual = primerNodo;
			boolean encontro = false;
			while(actual!=null && !encontro) {
				T prueba = actual.darObj();
				if (prueba.equals(obj)) 
					encontro = true;
				else
					actual = actual.darSiguiente();
			}
		}
		return actual;
	}

	public Nodo<T> getCurrentElement(T obj) {
		return current;
	}

	public void delete(T obj) {
		Nodo<T> eliminar =getElement(obj);
		if (eliminar!=null) {
			Nodo<T> anterior = eliminar.darAnterior();
			Nodo<T> siguiente = eliminar.darSiguiente();
			if (anterior ==null) {
				primerNodo =siguiente;
				siguiente.cambiarAnterior(null);
			} else if (siguiente ==null) {
				anterior.cambiarSiguiente(siguiente);
			} else {
				anterior.cambiarSiguiente(siguiente);
				siguiente.cambiarAnterior(anterior);
			}
		}
	}

	public void deleteAtK(int k) {
		Nodo<T> actual = primerNodo;
		boolean encontro = false;
		int i = 0;
		while(actual!=null && !encontro) {
			if (k==i) 
				encontro = true;
			else
				actual = actual.darSiguiente();
			i++;
		}
		if (actual != null) {
			Nodo<T> anterior = actual.darAnterior();
			Nodo<T> siguiente = actual.darSiguiente();
			if (anterior ==null) {
				primerNodo =siguiente;
				siguiente.cambiarAnterior(null);
			} else if (siguiente ==null) {
				anterior.cambiarSiguiente(siguiente);
			} else {
				anterior.cambiarSiguiente(siguiente);
				siguiente.cambiarAnterior(anterior);
			}
		}
	}	

	@Override
	public Nodo<T> next() {
		return current.darSiguiente();
	}

	@Override
	public Nodo<T> previous() {
		return current.darAnterior();
	}
}
