package model.data_structures;

public class Nodo<T> {

	//----------------------------------------------------------------------
	// Atributos.
	//----------------------------------------------------------------------

	private Nodo<T>  anterior;

	private Nodo<T>  siguiente;

	private T obj;

	//----------------------------------------------------------------------
	// Constructor.
	//----------------------------------------------------------------------

	public Nodo( T pObj ) {
		siguiente = null;
		anterior = null;
		obj = pObj;
	}

	//----------------------------------------------------------------------
	// Metodos.
	//----------------------------------------------------------------------

	public Nodo<T>  darAnterior() {
		return anterior;
	}

	public Nodo<T>  darSiguiente() {
		return siguiente;
	}

	public void cambiarAnterior(Nodo<T> pAnterior) {
		anterior = pAnterior;
	}

	public void cambiarSiguiente(Nodo<T> pSiguiente) {
		siguiente = pSiguiente;
	}

	public T darObj() {
		return obj;	
	}

}
