package model.logic;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import api.IDivvyTripsManager;
import model.vo.VOStation;
import model.vo.VOTrip;
import model.data_structures.*;

@SuppressWarnings({"unchecked", "rawtypes" })
public class DivvyTripsManager implements IDivvyTripsManager {

	private DoublyLinkedList listaTrips;

	private IDoublyLinkedList listaEstaciones;

	/**
	 * Carga una lista de estaciones en la lista listaTrips al leer un archivo .csv
	 */
	public <T> void loadStations (String stationsFile) {

		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		listaEstaciones = new DoublyLinkedList<>();
		int i =-1;

		try {
			br = new BufferedReader(new FileReader(stationsFile));
			while ((line = br.readLine()) != null) {
				i++;
				// use comma as separator
				String[] info = line.split(cvsSplitBy);
				if (info.length==7 && info[0].charAt(0)!='i') {
					int id =Integer.parseInt(info[0]);
					String name =info[1];
					String city =info[2];
					double latitude =Double.parseDouble(info[3]);
					double longitude =Double.parseDouble(info[4]);
					int dpcapacity =Integer.parseInt(info[5]);
					String online_date =info[6];

					T obj = (T) new VOStation(id, name, city, latitude, longitude, dpcapacity, online_date); 
					Nodo nuevo = new Nodo(obj);
					listaEstaciones.add(nuevo);
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		System.out.println("Se cargaron "+listaEstaciones.getSize()+ " elementos de un total de "+ i);
	}

	/**
	 * Carga una lista de viajes en la lista listaTrips al leer un archivo .csv
	 */
	public <T> void loadTrips (String tripsFile) {

		BufferedReader br = null;
		String line = "";
		String cvsSplitBy = ",";
		listaTrips = new DoublyLinkedList<>();
		int i=-1;

		try {
			br = new BufferedReader(new FileReader(tripsFile));
			while ((line = br.readLine()) != null) {
				i++;
				// use comma as separator
				String[] info = line.split(cvsSplitBy);
				if(info[0].charAt(0)=='1') {
					int trip_id =Integer.parseInt(info[0]);
					String start_time =info[1];
					String end_time =info[2];
					int bikeid =Integer.parseInt(info[3]);
					int tripduration =Integer.parseInt(info[4]);
					int from_station_id =Integer.parseInt(info[5]);
					String from_station_name =info[6];
					int to_station_id =Integer.parseInt(info[7]);
					String to_station_name =info[8];
					String usertype =info[9];
					String gender =null;
					int birthyear =0;

					if(info.length==10) {
						gender =null;
						birthyear =0;
					}else if(info.length==11) {
						gender =info[10];
						birthyear =0;
					}else if(info.length==12) {
						gender =info[10];
						birthyear =Integer.parseInt(info[11]);
					} 

					T obj = (T) new VOTrip(trip_id,start_time,end_time,bikeid,tripduration,from_station_id,from_station_name,to_station_id,to_station_name,usertype,gender,birthyear); 
					Nodo nuevo = new Nodo(obj);
					listaTrips.add(nuevo);

				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		System.out.println("Se cargaron "+listaTrips.getSize()+ " elementos de un total de "+ i);
	}

	/**
	 * 
	 */
	public IDoublyLinkedList <VOTrip> getTripsOfGender (String gender) {
		IDoublyLinkedList listaGenero = new DoublyLinkedList<>();

		if (listaTrips!=null) {
			Nodo actual = listaTrips.darPrimerNodo();
			VOTrip viaje = (VOTrip) actual.darObj();
			while(actual!=null) {
				viaje = (VOTrip) actual.darObj();
				if (viaje.darGender()!=null) {
					if (viaje.darGender().equals(gender)) {
						Nodo nuevo = new Nodo(viaje);
						listaGenero.add(nuevo);
					}
				}
				actual = actual.darSiguiente();
			}
		}
		return listaGenero;
	}

	public IDoublyLinkedList <VOTrip> getTripsToStation (int stationID) {
		IDoublyLinkedList listaViajes = new DoublyLinkedList<>();

		if (listaTrips!=null) {
			Nodo actual = listaTrips.darPrimerNodo();
			VOTrip viaje = (VOTrip) actual.darObj();
			while(actual!=null) {
				viaje = (VOTrip) actual.darObj();
				if (viaje.darEstacion()==stationID) {
					Nodo nuevo = new Nodo(viaje);
					listaViajes.add(nuevo);
				}
				actual = actual.darSiguiente();
			}
		}
		return listaViajes;
	}	
















}
