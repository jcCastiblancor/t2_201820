package model.vo;

/**
 * Representation of a Trip object
 */
@SuppressWarnings({"unused"})
public class VOTrip<T> implements Comparable <T>{

	private int trip_id;

	private String start_time;

	private String end_time;

	private int bikeid;

	private int tripduration;

	private int from_station_id;

	private String from_station_name;

	private int to_station_id;

	private String to_station_name;

	private String usertype;

	private String gender;

	private int birthyear;

	/**
	 * Crea un nuevo VOTrip.
	 * @param pTrip_id
	 * @param pStart_time
	 * @param pEnd_time
	 * @param pBikeid
	 * @param pTripduration
	 * @param pFrom_station_id
	 * @param pFrom_station_name
	 * @param pTo_station_id
	 * @param pTo_station_name
	 * @param pUsertype
	 * @param pGender
	 * @param pBirthyear
	 */
	public VOTrip(int pTrip_id, String pStart_time, String pEnd_time, int pBikeid, int pTripduration, int pFrom_station_id, String pFrom_station_name, 
			int pTo_station_id, String pTo_station_name, String pUsertype, String pGender, int pBirthyear) {

		trip_id = pTrip_id;
		start_time = pStart_time;
		end_time = pEnd_time;
		bikeid = pBikeid;
		tripduration = pTripduration;
		from_station_id = pFrom_station_id;
		from_station_name = pFrom_station_name;
		to_station_id = pTo_station_id;
		to_station_name = pTo_station_name;
		usertype = pUsertype;
		gender = pGender;
		birthyear = pBirthyear;
	}

	/**
	 * @return id - Trip_id
	 */
	public int id() {
		return trip_id;
	}	

	/**
	 * @return time - Time of the trip in seconds.
	 */
	public double getTripSeconds() {
		return tripduration;
	}

	/**
	 * @return station_name - Origin Station Name .
	 */
	public String getFromStation() {
		return from_station_name;
	}

	/**
	 * @return station_name - Destination Station Name .
	 */
	public String getToStation() {
		return to_station_name;
	}

	/**
	 * @return to_station_id - Station_id
	 */
	public int darEstacion() {
		return to_station_id;
	}

	/**
	 * @return gender - genero.
	 */
	public String darGender() {
		return gender;
	}

	public int compareTo(T o) {
		return 0;
	}
}
