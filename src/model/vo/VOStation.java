package model.vo;
/**
 * Representation of a Station object.
 */
@SuppressWarnings({"unused"})
public class VOStation<T> implements Comparable <T> {

	private int id;

	private String name;

	private String city;

	private double latitude;

	private double longitude;

	private int dpCapacity;

	private String onlineDate;

	/**
	 * Crea un nuevo Objeto VOStation.
	 * @param pId
	 * @param pName
	 * @param pCity
	 * @param pLatitude
	 * @param pLongitude
	 * @param pDpCapacity
	 * @param pOnlineDate
	 */

	public VOStation(int pId, String pName, String pCity, double pLatitude, double pLongitude, int pDpCapacity, String pOnlineDate) {
		id = pId;
		name=pName;
		city = pCity;
		latitude = pLatitude;
		longitude = pLongitude;
		dpCapacity = pDpCapacity;
		onlineDate = pOnlineDate;
	}

	/**
	 * @return id_Station -Station_id
	 */
	public int id() {
		return id;
	}

	public int compareTo(T o) {
		return 0;
	}
}