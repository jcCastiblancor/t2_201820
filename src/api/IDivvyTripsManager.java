package api;

import model.data_structures.IDoublyLinkedList;
import model.vo.VOTrip;

/**
 * Basic API for testing the functionality of the STS manager
 */
@SuppressWarnings({ "rawtypes" })
public interface IDivvyTripsManager {

	/**
	 * Method to load the Divvy trips of the STS
	 * @param tripsFile - path to the file 
	 */
	<T> void loadTrips(String tripsFile);


	/**
	 * Method to load the Divvy Stations of the STS
	 * @param stationsFile - path to the file 
	 */
	<T> void loadStations(String stationsFile);


	public IDoublyLinkedList <VOTrip> getTripsOfGender (String gender);


	public IDoublyLinkedList <VOTrip> getTripsToStation (int stationID);



}
