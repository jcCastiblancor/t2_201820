package test;

import org.junit.Before;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.Random;
import org.junit.Test;

import model.data_structures.*;
import model.vo.VOStation;

@SuppressWarnings({"unchecked", "rawtypes" })
public class IDoublyLinkedListTest<T> {

	// -------------------------------------------------------------
	// Atributos
	// -------------------------------------------------------------

	private DoublyLinkedList lista1;

	private DoublyLinkedList lista2;

	private DoublyLinkedList lista3;

	// -------------------------------------------------------------
	// Escenarios
	// -------------------------------------------------------------

	@Before
	public void setupEscenario1( )
	{
		lista1 = new DoublyLinkedList();
	}

	@Before
	public void setupEscenario2( )
	{
		lista2 = new DoublyLinkedList();

		Random rand = new Random();
		int  n = rand.nextInt(50) + 1;

		for(int j = 0; j<1000; j++) {
			T obj1 = (T) new VOStation(j, "", "", j*2/3, j*0.35, n, "");

			Nodo nuevo = new Nodo(obj1);
			lista2.add(nuevo);
			n = rand.nextInt(50) + 1;
		}
	}

	@Before
	public void setupEscenario3( )
	{
		lista3 = new DoublyLinkedList();

		T obj1 = (T) new VOStation(1, "Andes", "Bogot�", 50.4, 37.2, 37, "20/08/2018");
		Nodo nuevo = new Nodo(obj1);
		lista3.add(nuevo);

		obj1 = (T) new VOStation(2, "El poblado", "Medellin", 0.454, 11.45, 15, "29/10/2016");
		nuevo = new Nodo(obj1);
		lista3.add(nuevo);

		obj1 = (T) new VOStation(3, "Salitre", "Bogot�", -78.456, 25.432, 67, "05/11/2008");
		nuevo = new Nodo(obj1);
		lista3.add(nuevo);

		obj1 = (T) new VOStation(4, "Brooklyn", "NYC", 34.78, -21.5, 99, "17/09/2013");
		nuevo = new Nodo(obj1);
		lista3.add(nuevo);

		obj1 = (T) new VOStation(5, "Parque el Virrey", "Bogot�", 16.45, -1.78, 40, "01/01/2015");
		nuevo = new Nodo(obj1);
		lista3.add(nuevo);

		obj1 = (T) new VOStation(6, "Parque Lleras", "Medellin", 56.74, 15.2, 37, "7/10/2012");
		nuevo = new Nodo(obj1);
		lista3.add(nuevo);
	}

	// -------------------------------------------------------------
	// M�todos de prueba
	// -------------------------------------------------------------

	@Test
	public void testGetSize() {
		assertTrue( "El tama�o de la lista deberia ser igual a 0.", lista1.getSize()==0 );
		assertTrue( "El tama�o de la lista deberia ser igual a 1000.", lista2.getSize()==1000 );
		assertTrue( "El tama�o de la lista deberia ser igual a 1000.", lista3.getSize()==6 );
	}

	@Test
	public void testAdd() {
		T obj1 = (T) new VOStation(1, "inicio", "inicio", 50.4, 37.2, 37, "20/08/2018");
		Nodo nuevo = new Nodo(obj1);
		lista1.add(nuevo);
		assertTrue( "No se agrego en la primera posici�n", lista1.darPrimerNodo().darObj().equals(obj1));
		assertFalse( "No se agrego ningun elemento.", lista1.getSize()==0 );
	}

	@Test
	public void testAddAtEnd() {

		T obj1 = (T) new VOStation(10, "final", "final", 50.4, 37.2, 37, "20/08/2018");
		Nodo nuevo = new Nodo(obj1);
		lista2.addAtEnd(nuevo);
		Nodo ultimo = darUltimoNodo(lista2);

		assertTrue( "No se agrego en la ultima posicion", ultimo.darObj().equals(obj1));
		assertFalse( "No se agrego ningun elemento.", lista2.getSize()==1000 );
	}

	@Test
	public void testAddAtK() {

		T obj1 = (T) new VOStation(5, "medio", "medio", 50.4, 37.2, 37, "20/08/2018");
		Nodo nuevo = new Nodo(obj1);
		lista3.addAtK(nuevo, 3);
		Nodo nodoK = darNodoK(lista3, 3);
		assertTrue( "No se agrego en la posici�n k", nodoK.darObj().equals(obj1));
		assertFalse( "No se agrego ningun elemento.", lista2.getSize()==6 );
	}

	@Test
	public void testGetElement() {

		T obj1 = (T) lista3.darPrimerNodo().darObj();

		Nodo nodo = lista3.getElement((Comparable) obj1);

		assertTrue("Deberia de encontrar el objeto", nodo.darObj().equals(obj1)) ;

		T objE = (T) new VOStation(4535, "No", "Existe", 50.4, 37.2, 37, "20/08/2018");

		assertNull("Deberia de retornar nulo",lista3.getElement((Comparable)objE));

	}

	@Test
	public void testGetCurrentElement() {
	}

	@Test
	public void testDelete() {
		T objE = (T) new VOStation(13, "Borrar", "Eliminar", 50.4, 37.2, 37, "20/08/2018");
		Nodo nuevo = new Nodo(objE);
		lista3.add(nuevo);
		lista3.delete((Comparable) objE);
		assertTrue("No se elimino el objeto de la primera posicion", lista3.getSize()==6);

		objE = (T) new VOStation(13, "Borrar", "Eliminar", 50.4, 37.2, 37, "20/08/2018");
		nuevo = new Nodo(objE);
		lista3.addAtEnd(nuevo);
		lista3.delete((Comparable) objE);
		assertTrue( "No se elimino el objeto de la ultima posicion", lista3.getSize()==6);

		objE = (T) new VOStation(13, "Borrar", "Eliminar", 50.4, 37.2, 37, "20/08/2018");
		nuevo = new Nodo(objE);
		lista3.addAtK(nuevo, 3);
		lista3.delete((Comparable) objE);
		assertTrue( "No se elimino el objeto de la posicion k", lista3.getSize()==6);

		objE = (T) new VOStation(13, "Borrar", "Eliminar", 50.4, 37.2, 37, "20/08/2018");
		lista3.delete(6);
		assertTrue( "No se deberia eliminar nada, k es muy grande", lista3.getSize()==6);
	}

	@Test
	public void testDeleteAtK() {
		T objE = (T) new VOStation(13, "Borrar", "Eliminar", 50.4, 37.2, 37, "20/08/2018");
		Nodo nuevo = new Nodo(objE);
		lista3.add(nuevo);
		lista3.deleteAtK(0);
		assertTrue("No se elimino el objeto de la primera posicion", lista3.getSize()==6);

		objE = (T) new VOStation(13, "Borrar", "Eliminar", 50.4, 37.2, 37, "20/08/2018");
		nuevo = new Nodo(objE);
		lista3.addAtEnd(nuevo);
		lista3.deleteAtK(6);
		assertTrue( "No se elimino el objeto de la ultima posicion", lista3.getSize()==6);

		objE = (T) new VOStation(13, "Borrar", "Eliminar", 50.4, 37.2, 37, "20/08/2018");
		nuevo = new Nodo(objE);
		lista3.addAtK(nuevo, 3);
		lista3.deleteAtK(3);
		assertTrue( "No se elimino el objeto de la posicion k", lista3.getSize()==6);

		lista3.deleteAtK(6);
		assertTrue( "No se deberia eliminar nada, k es muy grande", lista3.getSize()==6);
	}

	private Nodo darUltimoNodo(DoublyLinkedList lista) {
		Nodo<T> actual = lista.darPrimerNodo();
		if (actual != null) {
			while (actual.darSiguiente()!=null) {
				if (actual.darSiguiente()!=null) {
					actual = actual.darSiguiente();
				}
			}
		}
		return actual;
	}

	private Nodo darNodoK(DoublyLinkedList lista, int k) {
		Nodo<T> actual = lista.darPrimerNodo();
		boolean agregado = false;
		int i = 0;
		while (actual!=null && !agregado) {
			if (i==k) {
				agregado = true;
			}
			actual = actual.darSiguiente();
			i++;
		}
		return actual;
	}
}
